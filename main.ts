export {}

//#region Hello World
let message = 'Goodbye World Again';
console.log(message);
//#endregion

//#region Variables
let x = 10;
const y = 20;
let sum;
const title = 'My Title'
//#endregion

//#region Typed Variables
let isBegginer: boolean = true;
let total: number = 0;
let name: string = 'Miguel';

let sentence: string = `Hey ${name} This is a really, really,
            really, really............ really long sentence!`;

console.log(sentence);

let n: null = null;
let u: undefined = undefined;

//#endregion

//#region Arrays
let list1: number[] = [1,2,3];
let list2: Array<number> = [1,2,3];

// Arrays - multiple types >> Tuples
let person1: [string, number] = ['Miguel', 47];

//#endregion

//#region enum
enum Color {Red, Green, Blue};
let c: Color = Color.Blue;
console.log(c);

let randomValue: any = 10;
randomValue = 'string value';
randomValue = 20;
randomValue = true;

//#endregion

//#region "any versus unknown" and object type

// let myVariable: any = 10;
let myVariable: any;

function hasName(obj: any): obj is {name: string} {
    return !!obj &&
            typeof obj === "object" && "name" in obj
}

myVariable = Object.create({name: "teste"});
if (hasName(myVariable))
{
    console.log(myVariable.name);
}

//(myVariable.name as string).toUpperCase();

//#endregion

//#region Multitype
let multiType: number | boolean;
multiType = true;
multiType = 20;
//multiType = 'teste string';   >> ERROR!

//#endregion

//#region Functions

// optional params come second
//function add(num1: number, num2?: number): number
function add(num1: number, num2: number = 10): number       // default parameters
{
    if (num2)
        return num1 + num2;
    else
        return num2;
}

let sumXY = add(5, 10);
console.log(sumXY);

sumXY = add(6);
console.log(sumXY);


//#endregion

//#region interface

interface Person {
    firstName: string;
    lastName?: string;
}

//v1
// function fullName(person: {firstName: string, lastName: string}): void {
//     console.log (`${person.firstName} ${person.lastName}`);
// }

//v2
function fullName(person: Person): void {
    console.log (`${person.firstName} ${person.lastName}`);
}

let p = {
    firstName: 'Bruce',
    lastName: 'Wayne'
} 

fullName(p);

//#endregion

//#region Class

class Employee {
    employeeName: string;

    /**
     *
     */
    constructor(name: string) {
        this.employeeName = name;
    }

    greet(){
        console.log(`Good morning ${this.employeeName}! How have you been?`);
    }

}

let emp1 = new Employee('Miguel');
console.log(emp1.employeeName);
emp1.greet();


//#endregion


//#region class - inheritance

class Manager extends Employee
{
    /**
     *
     */
    constructor(managerName: string) {
        console.log(`Manager ${managerName} created!`);
        super(managerName);
    }

    delegateWork(p: Employee) {
        console.log(`${this.employeeName} delegating work to ${p.employeeName}`);

    }

}

let manager1: Manager = new Manager('Nick Fury');
manager1.greet();
manager1.delegateWork(emp1);

//#endregion


//#region Access Modifiers

class AccessModifiers {

    private privateProp: string;
    public publicProp: string;
    protected protectedProp: string;    // Access by derived classes

}


//#endregion