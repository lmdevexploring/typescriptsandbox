"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//#region Hello World
let message = 'Goodbye World Again';
console.log(message);
//#endregion
//#region Variables
let x = 10;
const y = 20;
let sum;
const title = 'My Title';
//#endregion
//#region Typed Variables
let isBegginer = true;
let total = 0;
let name = 'Miguel';
let sentence = `Hey ${name} This is a really, really,
            really, really............ really long sentence!`;
console.log(sentence);
let n = null;
let u = undefined;
//#endregion
//#region Arrays
let list1 = [1, 2, 3];
let list2 = [1, 2, 3];
// Arrays - multiple types >> Tuples
let person1 = ['Miguel', 47];
//#endregion
//#region enum
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
let c = Color.Blue;
console.log(c);
let randomValue = 10;
randomValue = 'string value';
randomValue = 20;
randomValue = true;
//#endregion
//#region "any versus unknown" and object type
// let myVariable: any = 10;
let myVariable;
function hasName(obj) {
    return !!obj &&
        typeof obj === "object" && "name" in obj;
}
myVariable = Object.create({ name: "teste" });
if (hasName(myVariable)) {
    console.log(myVariable.name);
}
//(myVariable.name as string).toUpperCase();
//#endregion
//#region Multitype
let multiType;
multiType = true;
multiType = 20;
//multiType = 'teste string';   >> ERROR!
//#endregion
//#region Functions
// optional params come second
//function add(num1: number, num2?: number): number
function add(num1, num2 = 10) {
    if (num2)
        return num1 + num2;
    else
        return num2;
}
let sumXY = add(5, 10);
console.log(sumXY);
sumXY = add(6);
console.log(sumXY);
//v1
// function fullName(person: {firstName: string, lastName: string}): void {
//     console.log (`${person.firstName} ${person.lastName}`);
// }
//v2
function fullName(person) {
    console.log(`${person.firstName} ${person.lastName}`);
}
let p = {
    firstName: 'Bruce',
    lastName: 'Wayne'
};
fullName(p);
//#endregion
//#region Class
class Employee {
    /**
     *
     */
    constructor(name) {
        this.employeeName = name;
    }
    greet() {
        console.log(`Good morning ${this.employeeName}! How have you been?`);
    }
}
let emp1 = new Employee('Miguel');
console.log(emp1.employeeName);
emp1.greet();
//#endregion
//#region class - inheritance
class Manager extends Employee {
    /**
     *
     */
    constructor(managerName) {
        console.log(`Manager ${managerName} created!`);
        super(managerName);
    }
    delegateWork(p) {
        console.log(`${this.employeeName} delegating work to ${p.employeeName}`);
    }
}
let manager1 = new Manager('Nick Fury');
manager1.greet();
manager1.delegateWork(emp1);
//#endregion
//#region Access Modifiers
class AccessModifiers {
}
//#endregion
//# sourceMappingURL=main.js.map